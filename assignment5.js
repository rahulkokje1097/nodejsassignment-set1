var prompt = require('prompt');

prompt.start();

prompt.get(['lowerLimit', 'upperLimit'], function (err, result) {
    
    console.log('Command line input received.');
    let ll = parseInt(result.lowerLimit);
    let ul = parseInt(result.upperLimit)

    for(let num = ll; num<=ul; num++) {
        if(isPrime(num)) {
            console.log(`${num} is a prime number.`);
        }
    }
});

function isPrime(num) {
    if(num == 1) {
        return true;
    }
    for(let i = 2; i < num/2 ; i++) {
        if(num % i == 0) {
            return false;
        }
    }
    return true;
}