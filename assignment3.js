var prompt = require('prompt');

prompt.start();

prompt.get(['terms'], function (err, result) {
    
    console.log('Command line input received.');
    let num = parseInt(result.terms);
    console.log(`Febonacci series of ${num} terms:`);

    let n1 = 0;
    let n2 = 1;

    console.log(n1)
    console.log(n2)

    for(let i = 2; i <= num; i++) {
        console.log(n1 + n2);
        n2 = n1 + n2;
        n1 = n2 - n1;
    }
});