var prompt = require('prompt');

prompt.start();

prompt.get(['num1', 'num2', 'num3'], function (err, result) {
    
    console.log('Command line input received.');
    let max = 0;
    let num1 = parseInt(result.num1);
    let num2 = parseInt(result.num2);
    let num3 = parseInt(result.num3);

    max = (num1 > num2) ? (num1 > num3) ? num1 : num3 :(num2 > num3) ? num2 : num3;

    console.log(`Largest of the given 3 numbers is ${max}`);
});