var prompt = require('prompt');

prompt.start();

prompt.get(['num'], function (err, result) {
    
    console.log('Command line input received.');
    let num = parseInt(result.num);

    let fact= 1;
    while(num > 0) {
        fact = fact * num;
        num = num - 1;
    }

    console.log(`Factorial ${fact}`);
});