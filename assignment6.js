var prompt = require('prompt');

prompt.start();

prompt.get(['num'], function (err, result) {
    
    console.log('Command line input received.');
    let num = result.num;

    let numberOfDigits = num.length;
    let sum = 0;
    let temp = num;

    while (temp > 0) {

        let remainder = temp % 10;
        sum += remainder ** numberOfDigits;
        temp = parseInt(temp / 10);
    }

    if (sum == num) {
        console.log(`${num} is an Armstrong number`);
    } else {
        console.log(`${num} is not an Armstrong number.`);
    }
});