var prompt = require('prompt');

prompt.start();

prompt.get(['Year'], function (err, result) {
    
    console.log('Command line input received.');
    let year = parseInt(result.Year);

    if((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
        console.log(`${year} is a leap year.`);
    } else {
        console.log(`${year} is not a leap year.`);
    }
});