var prompt = require('prompt');

prompt.start();

prompt.get(['num'], function (err, result) {
    
    console.log('Command line input received.');
    let num = parseInt(result.num);
    console.log(`Table of ${num}:`);

    for(let i = 1; i <= 10; i++) {
        console.log(`${num} * ${i} = ${num*i}`);
    }
});